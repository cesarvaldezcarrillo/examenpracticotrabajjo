﻿using ExamenReclutamiento.Models;
using ExamenReclutamiento.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenReclutamiento.Data
{
    public class prospectoRepository
    {
        public bool GuardarProspecto(Prospectos prospecto ,List<Prospecto_Documentacion> lista_Documentos)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    db.Database.BeginTransaction();
                    var prospectoAdd = db.prospecto.Add(prospecto);
                    db.SaveChanges();
                    foreach (var item in lista_Documentos)
                    {
                        item.id_prospecto = prospecto.id;
                        db.prospecto_documentacion.Add(item);
                    }
                    db.SaveChanges();
                    db.Database.CommitTransaction();
                    return true;
                }
                catch (Exception ex)
                {
                    db.Database.BeginTransaction().Rollback();
                    return false;
                }
            }

        }

        public List<ProspectoInformacion> ListadoProspectos()
        {
            List<ProspectoInformacion> informacion = new List<ProspectoInformacion>();
            List<Prospectos> info = new List<Prospectos>();
            List<Prospecto_Documentacion> documentos = new List<Prospecto_Documentacion>();
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    info = (from b in db.prospecto
                            select b
                                ).ToList();
                    foreach (var item in info)
                    {
                        ProspectoInformacion data = new ProspectoInformacion();
                        documentos = (from b in db.prospecto_documentacion
                                select b
                                ).Where(x => x.id_prospecto == item.id)
                                .ToList();

                        data.InformacionProspecto = item;
                        data.DocumentosProsPecto = documentos;
                        informacion.Add(data);
                    }
                    return informacion;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }

        }

        public bool actualizarEstatus(VMProspecto collection)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    db.Database.BeginTransaction();
                    Prospectos info = db.prospecto.Where(x => x.id == collection.id).FirstOrDefault();
                    info.estatus = collection.estatus;
                    info.comentario = collection.comentario;
                    db.prospecto.Update(info);
                    db.SaveChanges();
                    db.Database.CommitTransaction();
                    return true;
                }
                catch (Exception ex)
                {
                    db.Database.BeginTransaction().Rollback();
                    return false;
                }
            }
        }
    }
}
