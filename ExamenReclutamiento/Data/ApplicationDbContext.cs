﻿using ExamenReclutamiento.Models;
using ExamenReclutamiento.Utils;
using Microsoft.EntityFrameworkCore;

namespace ExamenReclutamiento.Data
{
    public class ApplicationDbContext: DbContext//IdentityDbContext
    {
        AppConfiguration app = new AppConfiguration();
        public ApplicationDbContext()
        {
        }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> option) : base(option) { }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(app.ConnectionString); ;
        }
        public DbSet<Prospectos> prospecto { get; set; }
        public DbSet<Prospecto_Documentacion> prospecto_documentacion { get; set; }
    }
}
