﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenReclutamiento.Models.ViewModel
{
    public class VMProspecto
    {
        public int id { get; set; }
        public string estatus { get; set; }
        public string comentario { get; set; }
    }
}
