﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenReclutamiento.Models
{
    public class Prospecto_Documentacion
    {
        public int id { get; set; }
        public int id_prospecto { get; set; }
        public string nombre_archivo { get; set; }
        public string archivo { get; set; }
    }
}
