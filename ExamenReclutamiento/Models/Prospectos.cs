﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenReclutamiento.Models
{
    public class Prospectos
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string aPaterno { get; set; }
        public string aMaterno { get; set; }
        public string calle { get; set; }
        public string numero { get; set; }
        public string colonia { get; set; }
        public string codPostal { get; set; }
        public string telefono { get; set; }
        public string rfc { get; set; }
        public string estatus { get; set; }// Enviado, Autorizado o Rechazado
        public string comentario { get; set; }
    }
}
