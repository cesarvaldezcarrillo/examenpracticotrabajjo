﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenReclutamiento.Models
{
    public class ProspectoInformacion
    {
        public Prospectos InformacionProspecto { get; set; }
        public List<Prospecto_Documentacion> DocumentosProsPecto { get; set; }
    }
}
