﻿App.controller('FuncionesController', function ($scope, $http, $location) {
    $scope.documentoLista = [{ Nombre: '', Archivo: null }];

    $scope.getNumber = function (num) {
        return new Array(num);
    }
    $scope.AgregarDocumento = function () {
        $scope.documentoLista.push({ Nombre: '', Archivo:null });
    }
    $scope.EliminarDocumento = function (index) {

        $scope.documentoLista.splice(index, 1);
    }

    $scope.GuardarInformacion = function () {
        if ($scope.documentoLista.length == 0) {
            $scope.AgregarDocumento();
            Swal.fire(
                'Prospecto',
                'Debe cargar al menos un archivo.',
                'error'
            )
            return;
        } 
        var formData = new FormData(document.getElementById("form"));
        for (var i = 0; i < $scope.documentoLista.length; i++) {
            formData.append("archivos_nombres" + i, $scope.documentoLista[i].Nombre)
        }
       

        $http.post("/GuardarProspecto", formData, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        }).then(function successCallback(response) {
            if (response.data.success) {
                Swal.fire(
                    'Prospecto',
                    'Guardado Correctamente',
                    'success'
                ).then(function () {
                    window.location.reload();
                });
            } else {
                Swal.fire(
                    'Prospecto',
                    'Error al intentar Guardar la información',
                    'error'
                );
            }
            

        }, function errorCallback(response) {

        });
    };
    $scope.SalirProspecto = function () {
        Swal.fire({
            title: 'Prospecto',
            text: "Ningún dato será guardado, ¿Desea Salir?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Salir'
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = "/listado_prospecto";
            }
        })
    }


    $scope.ListadoProspectos = function () {
        $http.get("/ListadoProspectos").then(function successCallback(response) {
            $scope.prospectos = response.data.listadoProspectos;
        }, function errorCallback(response) {

        });
    }
    $scope.InfoProspecto = function (DatosProspecto, DocumentosProspecto) {
        $scope.infoProspecto = DatosProspecto;
        $scope.DocumentosProspecto = DocumentosProspecto;
        $("#informacionProspecto").modal();
    }
    $scope.CambioEstatus = function (DatosProspecto, estatus) {
        if (estatus == 'Rechazado') {
            Swal.fire({
                title: 'Rechazo de Prospectos',
                text: "Desea rechazar al prospecto: " + DatosProspecto.nombre + ' ' + DatosProspecto.aPaterno + ' ' + DatosProspecto.aMaterno,
                input: 'text',
                inputLabel: 'Observacion',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                inputValidator: (value) => {
                    if (!value) {
                        return 'Debe completar el motivo del rechazo.'
                    } else {
                        var params = {
                            id: DatosProspecto.id,
                            estatus: estatus,
                            comentario: value
                        };

                        $http.post("/AtualizarEstatus", params).then(function successCallback(response) {
                            if (response.data.success) {
                                Swal.fire({
                                    title:'Se ha rechazado al prospecto:',
                                    text: DatosProspecto.nombre + ' ' + DatosProspecto.aPaterno + ' ' + DatosProspecto.aMaterno,
                                    icon:'success'
                                }).then(function () {
                                        $scope.ListadoProspectos();
                                });
                            } else {
                                Swal.fire(
                                    'Prospecto',
                                    'Error al intentar Guardar la información',
                                    'error'
                                );
                            }
                        }, function errorCallback(response) {

                        });
                    }
                }
            })
        } else {


            Swal.fire({
                title: 'Aprobar Prospecto',
                text: "Desea aprobar al prospecto: " + DatosProspecto.nombre + ' ' + DatosProspecto.aPaterno + ' ' + DatosProspecto.aMaterno ,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
            }).then((result) => {
                if (result.isConfirmed) {
                    var params = {
                        id: DatosProspecto.id,
                        estatus: estatus,
                        comentario: ""
                    };

                    $http.post("/AtualizarEstatus", params).then(function successCallback(response) {

                        if (response.data.success) {
                            Swal.fire({
                                title:'Se ha aprobado al prospecto: ',
                                text: DatosProspecto.nombre + ' ' + DatosProspecto.aPaterno + ' ' + DatosProspecto.aMaterno+ " ",
                                icon:'success'
                            }).then(function () {
                                $scope.ListadoProspectos();
                            });
                        } else {
                            Swal.fire(
                                'Prospecto',
                                'Error al intentar Guardar la información',
                                'error'
                            );
                        }
                        
                        
                    }, function errorCallback(response) {

                    });
                }
            })

            
        }
    }
});
