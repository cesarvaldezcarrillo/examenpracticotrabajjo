﻿using ExamenReclutamiento.Data;
using ExamenReclutamiento.Models;
using ExamenReclutamiento.Models.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenReclutamiento.Controllers
{
    public class ProspectoController : Controller
    {
        private prospectoRepository prosRepository = new prospectoRepository();
        // GET: ProspectoController
        [HttpGet]
        [Route("/captura_prospeto")]
        public ActionResult Captura_prospeto()
        {
            string fileName = System.IO.Path.GetRandomFileName() + Guid.NewGuid().ToString() + ".tmp";
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Archivos", fileName);
            var filePath2 = Path.GetTempFileName();
            return View();
        }
        [Route("/GuardarProspecto")]
        [HttpPost]
        public async Task<IActionResult> Guardar()
        {
            var formData = await Request.ReadFormAsync();
            Prospectos prospecto = new Prospectos()
            {
                nombre = formData.FirstOrDefault(x => x.Key == "nombre").Value[0],
                aPaterno = formData.FirstOrDefault(x => x.Key == "aPaterno").Value[0],
                aMaterno = formData.FirstOrDefault(x => x.Key == "aMaterno").Value[0],
                calle = formData.FirstOrDefault(x => x.Key == "calle").Value[0],
                numero = formData.FirstOrDefault(x => x.Key == "numero").Value[0],
                colonia = formData.FirstOrDefault(x => x.Key == "colonia").Value[0],
                codPostal = formData.FirstOrDefault(x => x.Key == "codPostal").Value[0],
                telefono = formData.FirstOrDefault(x => x.Key == "telefono").Value[0],
                rfc = formData.FirstOrDefault(x => x.Key == "rfc").Value[0],
                estatus = "Enviado",
                comentario = ""
            };
            List<Prospecto_Documentacion> documentos = new List<Prospecto_Documentacion>();
            int contador = 0;
           
            foreach (var form in formData.Files)
            {
                string fileName = System.IO.Path.GetRandomFileName() + Guid.NewGuid().ToString() + ".tmp";
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Archivos", fileName);
                var pathString = Path.Combine(Directory.GetCurrentDirectory(), "Archivos");

                if (!Directory.Exists(pathString) )
                {
                    DirectoryInfo di = System.IO.Directory.CreateDirectory(pathString);
                }
                
                using (var stream = System.IO.File.Create(filePath))
                {
                    await form.CopyToAsync(stream);
                }
                Prospecto_Documentacion info = new Prospecto_Documentacion()
                {
                    nombre_archivo = formData.FirstOrDefault(x => x.Key == "archivos_nombres" + contador).Value[0],
                    archivo = filePath
                };
                documentos.Add(info);
                contador++;
            }
            bool resultado =  prosRepository.GuardarProspecto(prospecto, documentos);
            return Json(new
            {
                success = resultado
            });

        }
        [HttpGet]
        [Route("/listado_prospecto")]
        public ActionResult Listado_prospecto()
        {
            return View();
        }
        [HttpGet]
        [Route("/ListadoProspectos")]
        public JsonResult ListaProspecto()
        {
            List<ProspectoInformacion> resultado = prosRepository.ListadoProspectos();
            return Json(new
            {
                success = true,
                listadoProspectos = resultado
            });

        }

        [HttpGet]
        [Route("/evalucion_prospecto")]
        public ActionResult Evalucion_prospecto()
        {
            return View();
        }
        [Route("/AtualizarEstatus")]
        [HttpPost]
        public JsonResult ActulizarProspecto([FromBody] VMProspecto collection)
        {
            bool resultado = prosRepository.actualizarEstatus(collection);
            return Json(new
            {
                success = resultado
            });

        }

    }
}
