-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3309
-- Tiempo de generación: 16-03-2022 a las 04:23:22
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `examenreclutamiento`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prospecto`
--

DROP TABLE IF EXISTS `prospecto`;
CREATE TABLE IF NOT EXISTS `prospecto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  `aPaterno` varchar(250) NOT NULL,
  `aMaterno` varchar(250) DEFAULT NULL,
  `calle` varchar(250) NOT NULL,
  `numero` varchar(20) NOT NULL,
  `colonia` varchar(250) NOT NULL,
  `codPostal` varchar(50) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `rfc` varchar(100) NOT NULL,
  `estatus` enum('Enviado','Autorizado','Rechazado') NOT NULL DEFAULT 'Enviado',
  `comentario` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prospecto_documentacion`
--

DROP TABLE IF EXISTS `prospecto_documentacion`;
CREATE TABLE IF NOT EXISTS `prospecto_documentacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_prospecto` int(11) NOT NULL,
  `nombre_archivo` varchar(100) NOT NULL,
  `archivo` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
